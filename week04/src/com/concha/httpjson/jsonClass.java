package com.concha.httpjson;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class jsonClass {

    public static String httpToJSON(Map.Entry<String, List<String>> httpmap) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(httpmap);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }
}