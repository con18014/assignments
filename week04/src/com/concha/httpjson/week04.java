package com.concha.httpjson;
/*
* Used this as a guide to use Gson:
* https://stackoverflow.com/questions/1688099/converting-json-data-to-java-object
 */

import java.io.*;
import java.util.List;
import java.util.Map;
import com.google.gson.Gson;

public class week04 {
    public static void main(String[] args) throws IOException {

        Map<String, List<String>> m = httpClass.getHttpHeaders("http://www.google.com");
        String jsonFormat= "";
        httpClass http = new httpClass();

        for (Map.Entry<String, List<String>> entry : m.entrySet()) {
            try {
                jsonFormat = jsonClass.httpToJSON(entry);
                System.out.println(jsonFormat);
                //http = new Gson().fromJson(jsonFormat, httpClass.class);
                //System.out.println(http);
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }
}
