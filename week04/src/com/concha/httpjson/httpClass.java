package com.concha.httpjson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.w3c.dom.html.HTMLTableCaptionElement;

import java.net.HttpURLConnection;
import java.net.*;
import java.io.*;
import java.util.*;

public class httpClass {

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Map getHttpHeaders(String string) {

        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            hashmap = http.getHeaderFields();
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;
    }

    public String toString(){
        return "Key: " + key + "\nValue: " + value;
    }
}
