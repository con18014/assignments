/* Rodrigo Concha
 * CIT 360-03
 * BYU-Idaho
 * Winter 2021
 */

package jUnitTesting;

public class myJUnit {
    private String name;
    private String artist;
    private String genre;
    private int year;

    public myJUnit(String name, String artist, String genre, int year){
        this.name = name;
        this.artist = artist;
        this.genre = genre;
        this.year = year;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }
    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear(){return year;}

    public void setYear (int year) {
        this.year = year;
    }
}
