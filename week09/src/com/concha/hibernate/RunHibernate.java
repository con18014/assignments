package com.concha.hibernate;

import java.sql.*;
import java.util.*;

public class RunHibernate {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<Albums> c = t.getAlbums();
        for (Albums i : c) {
            System.out.println(i);
        }

        System.out.println(t.getAlbum(1));
        System.out.println(t.getAlbum(3));
        System.out.println(t.getAlbum(5));

        try
        {
            // Create a MySQL database connection
            String myDriver = "com.mysql.jdbc.Driver";
            String myUrl = "jdbc:mysql://localhost:3306/test?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "root", "miRamona9");

            Statement st = conn.createStatement();

            st.executeUpdate("INSERT INTO albums (id, name, artist, genre, year) "
                    +"VALUES (8, 'Automata', 'Between the Buried and Me', 'Progressive Metal', 2018)");

            System.out.println(t.getAlbum(8));

            conn.close();
        }
        catch (Exception e)
        {
            System.out.println("DATABASE ERROR!!!!");
            System.err.println(e.getMessage());
        }
    }
}