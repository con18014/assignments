package com.concha.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "albums")
public class Albums {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "artist")
    private String artist;

    @Column(name = "genre")
    private String genre;

    @Column(name = "year")
    private int year;

    public int getId() {return id;}
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }
    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear(){return year;}
    public void setYear (int year) {this.year = year;}

    public String toString() {
        return "Album ID number: " + Integer.toString(id) + "\nAlbum Name: " + name +
                "\nAlbum Artist: " + artist + "\nAlbum Genre: " + genre + "\nAlbum year: " + year + "\n\n\n";
    }
}