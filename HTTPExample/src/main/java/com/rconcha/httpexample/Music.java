package com.rconcha.httpexample;

/* Rodrigo Concha
 * CIT 360-03
 * BYU-Idaho
 * Winter 2021
 */

public class Music {

    private String name;
    private String album;
    private String year;
    private String genre;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String toString() {
        return "name: " + name + "\nalbum: " + album + "\nyear: " + year + "\ngenre: " + genre;
    }
}