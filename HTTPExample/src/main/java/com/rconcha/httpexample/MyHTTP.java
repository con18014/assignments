package com.rconcha.httpexample;

/* Rodrigo Concha
 * CIT 360-03
 * BYU-Idaho
 * Winter 2021
 */

/* Used this website as a major reference to create HTTP server
*  https://www.logicbig.com/tutorials/core-java-tutorial/http-server/http-server-basic.html
*/

import java.net.*;
import java.io.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;


public class MyHTTP {

    //Uses Object Mapper to convert Java Object into JSON format
    public static String objectToJSON(Music customer) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    //Uses Object Mapper to convert JSON string to Java Object
    public static Music JSONToObject(String json) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Music myObjects = null;

        try {
            myObjects = mapper.readValue(json, Music.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return myObjects;
    }

    //Gets HTTP content from web page and converts it to a string
    public static String getHttpContent(String string) {

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    //HTTP request handler
    private static void handleRequest(HttpExchange exchange) throws IOException {
        Music obj = new Music();
        obj.setAlbum("Grand Tour");
        obj.setName("Big Big Train");
        obj.setGenre("Progressive Rock");
        obj.setYear("2019");

        String response = MyHTTP.objectToJSON(obj);

        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    public static void main(String[] args) throws IOException {

        //Creates HTTP server that is open to listening
        HttpServer server = HttpServer.create(new InetSocketAddress(8500), 0);
        System.out.println("Starting server...");
        HttpContext context = server.createContext("/");
        context.setHandler(MyHTTP::handleRequest);
        server.start();

        //Takes JSON string from HTTP content and converts it to Java object
        String obj = MyHTTP.getHttpContent("http://localhost:8500/test");
        Music toObject = MyHTTP.JSONToObject(obj);
        System.out.println("--NEW JAVA OBJECT--");
        System.out.println(toObject);
    }
}