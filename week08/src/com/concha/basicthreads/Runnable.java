package com.concha.basicthreads;

import java.util.Random;

public class Runnable implements java.lang.Runnable {

    private String name;
    private int albumlength;
    private int sleep;
    private String album;
    private String artist;
    private int rand;

    public Runnable(String name, int albumlength, int sleep, String album, String artist) {

        this.name = name;
        this.albumlength = albumlength;
        this.sleep = sleep;
        this.album = album;
        this.artist = artist;

        Random random = new Random();
        this.rand = random.nextInt(600);
    }

    public void run() {
        System.out.println("\nExecuting with these parameters: Name = " + name + " Album Name = "
                + albumlength + " Sleep = " + sleep + " Rand Num = " + rand + "\n");
        for (int count = 1; count < rand; count++) {
            if (count % albumlength == 0) {
                System.out.println(name + " is listening to " + album + " by " + artist + ". It lasts " + albumlength + " minutes");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n" + name + " is done listening to " + album + ".\n");
    }
}