package com.concha.basicthreads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executable {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(2);

        Runnable listen1 = new Runnable("Bosco", 115, 1000, "Snow", "Spock's Beard");
        Runnable listen2 = new Runnable("Froid", 76, 500, "Kaleidoscope", "Transatlantic");
        Runnable listen3 = new Runnable("Hec", 76, 500, "Sola Scriptura", "Neal Morse");
        Runnable listen4 = new Runnable("Casto", 131, 1000, "Stardust We Are", "The Flower Kings");
        Runnable listen5 = new Runnable("Rod", 84, 500, "Terraformer", "Thank You Scientist");
        Runnable listen6 = new Runnable("Aldo", 68, 500, "Grimspound", "Big Big Train");
        Runnable listen7 = new Runnable("Machado", 57, 500, "Circus Pandemonium", "A.C.T.");

        myService.execute(listen1);
        myService.execute(listen2);
        myService.execute(listen3);
        myService.execute(listen4);
        myService.execute(listen5);
        myService.execute(listen6);
        myService.execute(listen7);

        myService.shutdown();
    }
}