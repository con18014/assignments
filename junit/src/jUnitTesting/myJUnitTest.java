/* Rodrigo Concha
 * CIT 360-03
 * BYU-Idaho
 * Winter 2021
 */

package jUnitTesting;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class myJUnitTest {

    @Test
    void getAlbumName() {
        myJUnit j = new myJUnit("Grand Tour", "Big Big Train", "Progressive Rock", 2019);
        assertEquals("Grand Tour", j.getName());
    }

    @Test
    void getAlbumArtist() {
        myJUnit j = new myJUnit("Grand Tour", "Big Big Train", "Progressive Rock", 2019);
        assertEquals("Big Big Train", j.getArtist());
    }

    @Test
    void getAlbumGenre() {
        myJUnit j = new myJUnit("Grand Tour", "Big Big Train", "Progressive Rock", 2019);
        assertEquals("Progressive Rock", j.getGenre());
    }

    @Test
    void getAlbumYear() {
        myJUnit j = new myJUnit("Grand Tour", "Big Big Train", "Progressive Rock", 2019);
        assertEquals(2019, j.getYear());
    }
}