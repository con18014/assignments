/* Rodrigo Concha
* CIT 360-03
* BYU-Idaho
* Winter 2021
*/

import java.util.*;

public class Week03validation {
    public static void main(String[] args) throws InputMismatchException {
        Scanner sc = new Scanner(System.in);
        float firstNumber = 0;
        float secondNumber = 0;
        boolean check = false;

        while (!check) {
            try {
                System.out.print("Enter a first number: ");
                firstNumber = sc.nextFloat();
                check = true;

            } catch (InputMismatchException e) {
                System.out.println("Enter a valid number.");
                sc.next();
            }
        }

        check = false;

        while (!check) {
            try {
                System.out.print("Enter a second number: ");
                secondNumber = sc.nextFloat();
                if (secondNumber == 0) {
                    System.out.println("Denominator cannot be zero.");
                    secondNumber = checkForZero(secondNumber);
                }
                check = true;
            } catch (InputMismatchException e) {
                System.out.println("Enter a valid number.");
                sc.next();
            }
        }

        System.out.println("First number: " + firstNumber + "\nSecond number: " + secondNumber);

        System.out.println("The result of dividing " + firstNumber + " by " + secondNumber + " is " + doDivision(firstNumber, secondNumber));

        sc.close();
    }

    public static float doDivision(float num1, float num2) {

        float result = num1 / num2;

        return result;
    }

    public static float checkForZero(float denominator) {
        float newDenominator = 0;
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        boolean check = false;

        while(!check) {
            try {
                System.out.print("Enter a valid denominator: ");
                newDenominator = sc.nextFloat();
                if (newDenominator == 0) {
                    System.out.println("Denominator cannot be zero.");
                }
                else {
                    check = true;
                }
            }
            catch (InputMismatchException e) {
                System.out.println("Enter a valid number.");
                sc.next();
            }
        }

        return newDenominator;

    }
}