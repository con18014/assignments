/* Rodrigo Concha
 * CIT 360-03
 * BYU-Idaho
 * Winter 2021
 *
 * This program is largely based on this:
 * https://bitbucket.org/tuckettt/mywebhelloworld/src/master/
 */

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/** This is a simple servlet that implements doPost and doGet.  The doPost takes
 *  three parameters (first, middle, and last name) and displays them.
 */
@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    /** this is the main method that uses the three parameters and displays them. */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String firstname = request.getParameter("firstname");
        String middlename = request.getParameter("middlename");
        String lastname = request.getParameter("lastname");
        out.println("<h1>Here is your full name</h1>");
        out.println("<p>First Name: " + firstname + "</p>");
        out.println("<p>Middle Name: " + middlename + "</p>");
        out.println("<p>Last Name: " + lastname + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }
}