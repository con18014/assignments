package concha.basiccollections;

public class Music {
    String artist;
    private String album;
    private String genre;

    public Music(String artist, String album, String genre) {
        this.artist = artist;
        this.album = album;
        this.genre = genre;
    }

    public String toString() {
        return "Title: " + artist + "\nAlbum: " + album + "\nGenre: " + genre + "\n";
    }
}