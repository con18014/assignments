package concha.basiccollections;

import java.util.Comparator;

//Sorting function
public class SortByArtist implements Comparator<Music> {
    public int compare(Music a, Music b)
    {
        return a.artist.compareTo(b.artist);
    }
}