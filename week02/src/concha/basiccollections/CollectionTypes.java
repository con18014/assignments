/*
 * This program was largely based on Brother Tuckett's tutorial:
 * https://www.youtube.com/watch?time_continue=737&v=NFMtOoTwpcs&feature=emb_title
 *
 * I used this website as a guide to implement Comparator requirement:
 * https://www.geeksforgeeks.org/comparator-interface-java/
 *
 * Rodrigo Concha
 * CIT 360-03
 * BYU-Idaho
 * Winter 2021
 */
package concha.basiccollections;

import java.util.*;

public class CollectionTypes {
    public static void main(String[] args) {

        System.out.println("Favorite Foods using different Collection Types");

        System.out.println("LIST");
        List list = new ArrayList();
        list.add("mom's enchiladas");
        list.add("Crown Burgers");
        list.add("Jimmy John's Gargantuan sandwich");
        list.add("Cafe Rio's shredded chicken salad");
        list.add("tacos al pastor");
        list.add("mom's enchiladas");
        list.add("The Pie pizza");
        list.add("Cold Stone's Oreo Overload");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("SET");
        Set set = new TreeSet();
        set.add("mom's enchiladas");
        set.add("Crown Burgers");
        set.add("Jimmy John's Gargantuan sandwich");
        set.add("Cafe Rio's shredded chicken salad");
        set.add("tacos al pastor");
        set.add("mom's enchiladas");
        set.add("The Pie pizza");
        set.add("Cold Stone's Oreo Overload");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("QUEUE");
        Queue queue = new PriorityQueue();
        queue.add("mom's enchiladas");
        queue.add("Crown Burgers");
        queue.add("Jimmy John's Gargantuan sandwich");
        queue.add("Cafe Rio's shredded chicken salad");
        queue.add("tacos al pastor");
        queue.add("mom's enchiladas");
        queue.add("The Pie pizza");
        queue.add("Cold Stone's Oreo Overload");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("MAP");
        Map map = new HashMap();
        map.put(1, "mom's enchiladas");
        map.put(2, "Crown Burgers");
        map.put(3, "Jimmy John's Gargantuan sandwich");
        map.put(4, "Cafe Rio's shredded chicken salad");
        map.put(5, "tacos al pastor");
        map.put(6, "mom's enchiladas");
        map.put(7, "The Pie pizza");
        map.put(5, "Cold Stone's Oreo Overload");

        for (int i = 1; i < 8; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("List of Favorite Albums using Generics:");
        List<Music> myList = new LinkedList<Music>();
        myList.add(new Music("Transatlantic", "The Whirlwind", "Progressive Rock"));
        myList.add(new Music("Dream Theater", "Scenes From a Memory", "Progressive Metal"));
        myList.add(new Music("Green Day", "Warning", "Punk Rock"));
        myList.add(new Music("Spock's Beard", "Snow", "Progressive Rock"));
        myList.add(new Music("The Aristocrats", "The Aristocrats", "Instrumental Rock"));

        for (Music albums : myList) {
            System.out.println(albums);
        }

        Collections.sort(myList, new SortByArtist());

        System.out.println("\n\nFavorite Albums sorted by Artist");
        for (int i=0; i<myList.size(); i++)
            System.out.println(myList.get(i));
    }
}